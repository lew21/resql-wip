from __future__ import annotations

from dataclasses import dataclass, field, asdict
from typing import Dict, List

USING_AIOPG = True
USING_AIOPG = False


@dataclass
class Field:
    name: str


@dataclass
class Column(Field):
    pass


@dataclass
class Relation(Field):
    to: Table
    keymap: Dict[int, int]


@dataclass
class Table:
    name: str
    columns: Dict[int, Column] = field(default_factory=dict)
    primary_key: List[int] = None
    fields: Dict[str, Field] = field(default_factory=dict)


async def discover_schema(conn) -> Dict[str, Table]:
    schema = {}

    async with conn.transaction():
        async for row in conn.cursor("""
                SELECT oid, relname
                FROM pg_class
                WHERE relnamespace = 'public'::regnamespace AND reltype != 0
                """):
            oid, relname = row
            schema[oid] = Table(relname)
        async for row in conn.cursor("""
                SELECT attrelid, attnum, attname
                FROM pg_attribute
                WHERE attrelid = any($1) AND attnum > 0 AND not attisdropped
                """, list(schema.keys())):
            attrelid, attnum, attname = row
            table = schema[attrelid]
            column = Column(attname)
            table.columns[attnum] = column
            table.fields[column.name] = column
        async for row in conn.cursor("""
                SELECT conrelid, contype, conname, conkey, confrelid, confkey
                FROM pg_constraint
                WHERE conrelid = any($1)
                """, list(schema.keys())):
            conrelid, contype, conname, conkey, confrelid, confkey = row
            table = schema[conrelid]

            if USING_AIOPG and hasattr(contype, 'encode'):
                contype = contype.encode('utf-8')

            if contype == b'p':
                table.primary_key = conkey
            elif contype == b'f':
                to = schema[confrelid]
                from_key = tuple(table.columns[colnum] for colnum in conkey)
                to_key = tuple(to.columns[colnum] for colnum in confkey)
                table.fields[conname] = Relation(conname, to, list(zip(from_key, to_key)))

    for k, table in schema.items():
        print(k, table.name, table.primary_key)
        for k, field in table.fields.items():
            print(f" {field.name}")

    return {table.name: table for table in schema.values()}


def quote_ident(ident):
    return f'''"{ident.replace('"', '""')}"'''


def parse_field_spec(source, field_spec):
    joins = set()
    field = Relation(source.name, source, None)
    prefix = ()
    for step in field_spec.split('/'):
        if isinstance(field, Relation):
            joins.add(prefix + (field.name,))
        prefix = prefix + (field.name,)
        field = field.to.fields[step]

    return (prefix, field, joins)


def sql_select(source: Table, fields: List[str], **filters):
    joins = {(source.name,)}

    sql_fields = []
    for field_spec in fields:
        prefix, field, new_joins = parse_field_spec(source, field_spec)
        joins = joins | new_joins

        if isinstance(field, Relation):
            raise RuntimeError("You cannot select a whole relation.")

        sql_fields.append(f'''{quote_ident('.'.join(prefix))}.{quote_ident(field.name)} as {quote_ident('/'.join((prefix + (field.name,))[1:]))}''')

    sql_filters = []
    args = []
    for field_spec, values in filters.items():
        prefix, field, new_joins = parse_field_spec(source, field_spec)
        joins = joins | new_joins

        sql_typecast = ''
        sql_alias = quote_ident('.'.join(prefix))

        if isinstance(field, Relation):
            sql_key = ', '.join(f'{sql_alias}.{quote_ident(f.name)}' for f, t in field.keymap)
            if not USING_AIOPG and len(field.to.primary_key) > 1:
                sql_typecast = f'::{field.to.name}_pkey_t[]'
        else:
            sql_key = f'{sql_alias}.{quote_ident(field.name)}'

        args.append(values)
        sql_filters.append(f'''({sql_key}) = any(${len(args)}{sql_typecast})''')

    sql_sources = []
    for join in sorted(joins):
        relation = Relation(None, source, [])
        for step in join[1:]:
            relation = relation.to.fields[step]

        sql_from_alias = quote_ident('.'.join(join[:-1]))
        sql_to_alias = quote_ident('.'.join(join))

        sql_source = f'''{quote_ident(relation.to.name)} {sql_to_alias}'''

        sql_conditions = [f'{sql_from_alias}.{quote_ident(f.name)} = {sql_to_alias}.{quote_ident(t.name)}' for f, t in relation.keymap]
        if sql_conditions:
            sql_source += f''' ON {' AND '.join(sql_conditions)}'''

        sql_sources.append(sql_source)

    sql = f"""SELECT {', '.join(sql_fields)}"""
    if sql_sources:
        sql += "\nFROM " + '\n  LEFT JOIN '.join(sql_sources)
    if sql_filters:
        sql += "\nWHERE " + '\n  AND '.join(sql_filters)

    return sql, args
