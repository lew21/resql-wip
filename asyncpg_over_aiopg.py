import aiopg
import re


class _Cursor:
    def __init__(self, cur, sql, *args):
        self.cur = cur
        self.sql = sql
        self.args = args
        self.started = False

    def __aiter__(self):
        return self

    async def __anext__(self):
        if not self.started:
            self.cur = await self.cur.__aenter__()
            await self.cur.execute(self.sql, self.args)
            self.started = True
        return await self.cur.__anext__()


class _Transaction:
    async def __aenter__(self):
        pass

    async def __aexit__(self, *args):
        pass


_vars = re.compile('\$[0-9]+')

def _fix_sql(sql):
    return _vars.sub('%s', sql)


class _Connection:
    def __init__(self, con):
        self.con = con

    async def execute(self, sql, *args):
        async with self.con.cursor() as cur:
            await cur.execute(_fix_sql(sql), args)

    def cursor(self, sql, *args):
        return _Cursor(self.con.cursor(), _fix_sql(sql), *args)

    def transaction(self):
        return _Transaction()

    async def fetchrow(self, sql, *args):
        async for row in self.cursor(sql, *args):
            return row


class _PoolAcquireContextManager:
    def __init__(self, pacm):
        self.pacm = pacm

    async def __aenter__(self):
        return _Connection(await self.pacm.__aenter__())

    async def __aexit__(self, *args):
        await self.pacm.__aexit__(*args)


class _Pool:
    def __init__(self, pool):
        self.pool = pool

    def acquire(self):
        return _PoolAcquireContextManager(self.pool.acquire())


async def create_pool(dsn):
    return _Pool(await aiopg.create_pool(dsn))
