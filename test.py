from __future__ import annotations

import asyncio
import os
import resql

if resql.USING_AIOPG:
    import asyncpg_over_aiopg as asyncpg
else:
    import asyncpg


async def main():
    pool = await asyncpg.create_pool(os.environ['DATABASE_URL'])
    async with pool.acquire() as conn:

        await conn.execute(
            """
            drop table if exists "tag";
            drop table if exists "category";
            drop table if exists "comment";
            drop table if exists "article";
            drop table if exists "user";

            create table "user" (
                "id" uuid default uuid_generate_v4() primary key,
                "name" varchar(150)
            );

            create table "article" (
                "author_id" uuid constraint "author" references "user",
                "id" uuid default uuid_generate_v4(),
                primary key ("author_id", "id"),
                "title" varchar(150),
                "body" text
            );

            drop type if exists "article_pkey_t";
            create type "article_pkey_t" as ("author_id" uuid, "id" uuid);

            create table "comment" (
                "article_author_id" uuid constraint "article_author" references "user",
                "article_id" uuid,
                constraint "article" foreign key ("article_author_id", "article_id") references "article",
                "id" uuid default uuid_generate_v4(),
                primary key ("article_author_id", "article_id", "id"),
                "author_id" uuid constraint "author" references "user",
                "body" text
            );

            drop type if exists "comment_pkey_t";
            create type "comment_pkey_t" as ("article_author_id" uuid, "article_id" uuid, "id" uuid);

            create table "category" (
                "id" uuid default uuid_generate_v4() primary key,
                "name" varchar(150),
                "creator_id" uuid constraint "creator" references "user"
            );

            create table "tag" (
                "category_id" uuid constraint "category" references "category",
                "article_author_id" uuid,
                "article_id" uuid,
                constraint "article" foreign key ("article_author_id", "article_id") references "article",
                primary key ("category_id", "article_author_id", "article_id")
            );

            drop type if exists "tag_pkey_t";
            create type "tag_pkey_t" as ("category_id" uuid, "article_author_id" uuid, "article_id" uuid);
            """
        )

        user_key = await conn.fetchrow("""insert into "user" ("name") values ('Linus') returning id""")
        article_key = await conn.fetchrow("""insert into "article" ("author_id", "title", "body") values ($1, 'Lorem ipsum', 'dolor sit amet') returning author_id, id""", *user_key)
        cat_latin_key = await conn.fetchrow("""insert into "category" ("name", "creator_id") values ('Latin', $1) returning id""", *user_key)
        cat_boring_key = await conn.fetchrow("""insert into "category" ("name") values ('Boring') returning id""")
        await conn.execute("""insert into "tag" ("category_id", "article_author_id", "article_id") values ($1, $2, $3)""", *cat_latin_key, *article_key)
        await conn.execute("""insert into "tag" ("category_id", "article_author_id", "article_id") values ($1, $2, $3)""", *cat_boring_key, *article_key)

        schema = await resql.discover_schema(conn)

        sql, args = resql.sql_select(schema['tag'], fields = ('category/name', 'category/creator/name', 'article/title',))
        print(sql, args)
        async with conn.transaction():
            async for row in conn.cursor(sql, *args):
                print(row)
        print()

        sql, args = resql.sql_select(schema['article'], fields = ('author/name',), author=[article_key[0]])
        print(sql, args)
        async with conn.transaction():
            async for row in conn.cursor(sql, *args):
                print(row)
        print()

        sql, args = resql.sql_select(schema['article'], fields = ('id',), author=[article_key[0]])
        print(sql, args)
        async with conn.transaction():
            async for row in conn.cursor(sql, *args):
                print(row)
        print()

        sql, args = resql.sql_select(schema['tag'], fields = ('category_id',), article=[tuple(article_key)])
        print(sql, args)
        async with conn.transaction():
            async for row in conn.cursor(sql, *args):
                print(row)
        print()

        sql, args = resql.sql_select(schema['article'], fields = ('title',), title=["Lorem ipsum"])
        print(sql, args)
        async with conn.transaction():
            async for row in conn.cursor(sql, *args):
                print(row)
        print()


asyncio.run(main())
